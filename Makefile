# Your program must compile with 'make'
# You must not change this file.
SHELL := /bin/bash
CC = gcc
FLAGS = -std=c99 -Wall -Werror -g -pedantic

default:
	$(CC) $(FLAGS) *.c -o rgrep

clean:
	rm -f rgrep

check: clean default

	test "`echo -e "ksdhlssdf" | ./rgrep '.{0,}'`" = "ksdhlssdf"
	test "`echo -e "ksdhlssdf" | ./rgrep '\.{1,}'`" = ""

	test "`echo -e "a\nb\nc" | ./rgrep 'a'`" = "a"
	test "`echo "a" | ./rgrep 'a{0,}a'`" = "a"
	test "`echo "a" | ./rgrep '...'`" = ""
	
	test "`echo "1 fine.txt" | ./rgrep ' ....{0,}\.txt'`" = "1 fine.txt"
	test "`echo "2 reallylong.txt" | ./rgrep ' ....{0,}\.txt'`" = "2 reallylong.txt"
	test "`echo "3 abc.txt" | ./rgrep ' ....{0,}\.txt'`" = "3 abc.txt"
	test "`echo "4 s.txt" | ./rgrep ' ....{0,}\.txt'`" = ""
	test "`echo "5 nope.pdf" | ./rgrep ' ....{0,}\.txt'`" = ""
	
	test "`echo "hey" | ./rgrep 'heyy{0,}'`" = "hey"
	test "`echo "heyy" | ./rgrep 'heyy{0,}'`" = "heyy"
	test "`echo "heyyy" | ./rgrep 'heyy{0,}'`" = "heyyy"
	
	test "`echo "hidden" | ./rgrep 'h.d.{1,}n'`" = "hidden"
	test "`echo "hidin" | ./rgrep 'h.d.{1,}n'`" = "hidin"
	test "`echo "hbdwabcdefgn" | ./rgrep 'h.d.{1,}n'`" = "hbdwabcdefgn"
	test "`echo "hadbn" | ./rgrep 'h.d.{1,}n'`" = "hadbn"

	test "`echo "cut" | ./rgrep 'cu\.{0,2}t'`" = "cut"
	test "`echo "cu.t" | ./rgrep 'cu\.{0,2}t'`" = "cu.t"
	test "`echo "cu..t" | ./rgrep 'cu\.{0,2}t'`" = "cu..t"
	test "`echo "cu...t" | ./rgrep 'cu\.{0,2}t'`" = ""

	test "`echo "ac" | ./rgrep 'ab{0,0}c'`" = "ac"
	test "`echo "c" | ./rgrep 'a{0,0}c'`" = "c"
	test "`echo "abc" | ./rgrep 'ab{0,0}c'`" = ""

	test "`echo "123hello" | ./rgrep 'hello'`" = "123hello"
	test "`echo "hello143" | ./rgrep 'hello'`" = "hello143"
	test "`echo "hello there" | ./rgrep 'hello'`" = "hello there"

	test "`echo "heeeeey" | ./rgrep 'he....y'`" = "heeeeey"
	test "`echo "he1234y" | ./rgrep 'he....y'`" = "he1234y"
	test "`echo "helolay" | ./rgrep 'he....y'`" = "helolay"

	test "`echo "re.ge\\x" | ./rgrep 're\\.ge\\\\x'`" = "re.ge\\x"

	test "`echo "jeeeeeeeeello" | ./rgrep 'je{0,}llo'`" = "jeeeeeeeeello"
	test "`echo "jllo" | ./rgrep 'je{0,}llo'`" = "jllo"

	test "`echo "tat" | ./rgrep 't.{1,}t'`" = "tat"
	test "`echo "tt" | ./rgrep 't.{1,}t'`" = ""
	test "`echo "taat" | ./rgrep 't.{1,}t'`" = "taat"
	test "`echo "tsdfsefsdt" | ./rgrep 't.{1,}t'`" = "tsdfsefsdt"
	test "`echo "tsiosdhfos" | ./rgrep 't.{1,}t'`" = ""

	test "`echo "aa" | ./rgrep 'as{0,2}a'`" = "aa"
	test "`echo "assa" | ./rgrep 'as{0,}sa'`" = "assa"
	test "`echo "assa" | ./rgrep 'as{0,2}sa'`" = "assa"
	test "`echo "assa" | ./rgrep 'as{0,2}a'`" = "assa"

	test "`echo "aaaaaa" | ./rgrep 'a{1,10}'`" = "aaaaaa"
	test "`echo "aaaaaa" | ./rgrep 'a{1,10}a{2,3}a{2,3}'`" = "aaaaaa"
	test "`echo "aaaaaa" | ./rgrep 'a{1,10}a'`" = "aaaaaa"
	test "`echo "asss" | ./rgrep 'as{0,2}s'`" = "asss"

	@echo "Passed sanity check."