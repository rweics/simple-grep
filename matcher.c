#include "matcher.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

int all_matches(char *line, char *pattern);
int dots_matches(char *line, char *pattern);
int bracket_matches(char *line, char *pattern);

/**
 * Implementation of your matcher function, which
 * will be called by the main program.
 *
 * You may assume that both line and pattern point
 * to reasonably short, null-terminated strings.
 */
int rgrep_matches(char *line, char *pattern) {
    if (!*line)
		return 0;
    if (all_matches(line, pattern))
        return 1;
    return  rgrep_matches(line+1, pattern);
}

int all_matches(char *line, char *pattern) {
	while (*pattern) {
        int escape = 0;
		if (*pattern == '\\' && *(pattern-1) != '\\') {
            escape = 1;
            pattern++;
		}
    	if (*pattern == '.' && !escape) {
			int len = dots_matches(line, pattern);
			if (!len)
				return 0;
			line += len;
			pattern += len;
		}
        if (*pattern == '{' && !escape) {
            int len = bracket_matches(line, pattern);
            if (!len) {
                return 0;
            }
            else return 1;
        }
        if (*pattern != *line && *(pattern+1) != '{')
            return 0;
		line++;
		pattern++;
	}
	return 1;
}

int dots_matches(char *line, char *pattern) {
    int i = 0;
	while (*pattern == '.') {
		if (!*line)
			return 0;
		i++;
		line++;
		pattern++;
	}
    return i;
}

int bracket_matches(char *line, char *pattern) {
    char *rep = pattern-1;
	int i = 0;
    pattern++;
    line--;
    char min_rep[20], max_rep[20], *comma = pattern, *close_bracket = pattern;
    while (*comma != ',')
        comma++;
    while (*close_bracket != '}')
        close_bracket++;
    strncpy(min_rep, pattern, comma-pattern);
    min_rep[comma-pattern] = '\0';
    strncpy(max_rep, comma+1, close_bracket-comma-1);
    max_rep[close_bracket-comma-1] = '\0';
    int min = atoi(min_rep), max = atoi(min_rep)+atoi(max_rep);
    if (*rep == '.' && *(rep-1) != '\\') {
    	if (strlen(line) < min)
            return 0;
        else return rgrep_matches(line+min, close_bracket+1) ? 1 : 0;
    } else {
    	while (*line == *rep) {
    		if (!*line)
				return 0;
            line++;
			i++;
    	}
        if (*(comma+1)=='}') {
            return i >= min && rgrep_matches(line+min-1, comma+2) ? 1 : 0;
        }
        return i >= min && i <= max && rgrep_matches(line+min-1, close_bracket+1) ? 1 : 0;
    }
}